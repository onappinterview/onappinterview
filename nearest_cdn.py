#!/usr/bin/env python

"""nearest_cdn.py: Calculate nearest CDN server."""

__author__ = "Mohd Rozi Za'far"

import geoip2.database
import ipaddr
from geopy.distance import great_circle


# Reference
#
# 'Mumbai - Singapore': 3921,
# 'Singapore - Tokyo': 5312,
# 'Singapore - Los Angeles': 14112,
# 'Tokyo - Los Angeles': 8810,
# 'Los Angeles - London': 8750,
# 'Los Angeles - Sao Paulo': 9900,


class NearestCDN:
    def __init__(self):
        # predefined routes, to speedup calculation
        self.best_routes = {
            # Mumbai,Singapore + Singapore,Los Angeles
            'Mumbai': (18033, 'Mumbai - Singapore -> Singapore - Los Angeles', 'Los Angeles'),
            'Singapore': (14112, 'Singapore - Los Angeles', 'Los Angeles'),
            'Tokyo': (8810, 'Tokyo - Los Angeles', 'Los Angeles'),
            'Los Angeles': (0, 'Los Angeles - London', 'Los Angeles'),  # webserver
            'London': (0, 'Los Angeles - London', 'London'),  # webserver
            'Sao Paulo': (9900, 'Los Angeles - Sao Paulo', 'Los Angeles'),
        }

        # predefined coordinates, to speedup query
        self.location = {
            'Mumbai': (19.075984, 72.877656),
            'Singapore': (1.352083, 103.819836),
            'Tokyo': (35.689487, 139.691706),
            'Los Angeles': (34.052234, -118.243685),
            'London': (51.507351 - 0.127758),
            'Sao Paulo': (-23.550520, -46.633309)}

        # cache all IPs
        self.location_cache = {}  # {(ip, nearest_backbone, total_distance),(..,..)}
        self.ip = ''
        self.geoip_reader = geoip2.database.Reader('GeoLite2-City.mmdb')

    def set_ip(self, ip):
        try:
            ip = ipaddr.IPAddress(ip)
        except ValueError:
            print " => Invalid IP address."
            exit(1)
        self.ip = ip

    def get_nearest_backbone(self):
        try:
            res = self.geoip_reader.city(self.ip)
        except geoip2.errors.AddressNotFoundError:
            print " => Cannot identify the location of {}.".format(self.ip)
            exit(1)
        nearest_backbone = (0, '')  # (distance, location_name)

        # for each backbone, get the nearest distance
        for location_name, location_coordinates in self.location.iteritems():
            distance = great_circle(location_coordinates, (res.location.latitude, res.location.longitude)).km
            if nearest_backbone[0] == 0:
                nearest_backbone = (distance, location_name)
            else:
                if distance < nearest_backbone[0]:
                    nearest_backbone = (distance, location_name)
        return nearest_backbone

    def calculate(self):
        if self.ip in self.location_cache:  # grab from the cache first
            location_cache = self.location_cache[self.ip]
            backbone_name = location_cache[1]
            total_distance = location_cache[2]
            print '(from cache)'
            return (total_distance, backbone_name)

        # if the ip not in the cache, continue with calculation
        nearest_backbone = self.get_nearest_backbone()
        distance_to_nearest_backbone = nearest_backbone[0]
        backbone_name = nearest_backbone[1]
        total_distance = distance_to_nearest_backbone + self.best_routes[backbone_name][0]
        self.location_cache[self.ip] = (self.ip, backbone_name, total_distance)

        return (total_distance, backbone_name)

    def get_input(self):
        while True:
            ip = raw_input("Please enter an IP address: ")
            self.set_ip(ip.strip())
            self.display_info()

    def display_info(self):
        total_distance, backbone_name = self.calculate()
        print ' => {}, {}km'.format(self.best_routes[backbone_name][2], int(total_distance))


cdn = NearestCDN()
cdn.get_input()  # get input from stdin
