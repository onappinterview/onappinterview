#!/usr/bin/env python

"""stream_processing: Get top 5 of most usage bytes."""

__author__ = "Mohd Rozi Za'far"

import StringIO
import csv
from itertools import groupby
from operator import itemgetter
from threading import _Timer


class Timer(_Timer):
    def run(self):
        while not self.finished.is_set():
            self.finished.wait(self.interval)
            self.function(*self.args, **self.kwargs)
        self.finished.set()


class BytesReport:
    def __init__(self):
        self.data = []  # [(datetime,ip,hostname,bytes_served), (.., .., .., ..)]
        self.ips = {}  # {ip: hostname, ..: ..}

    def insert_item(self, datetime, ip, hostname, bytes_served):
        self.data.append((datetime, ip, hostname, bytes_served))
        self.ips[ip] = hostname

    def simulate_streaming_input(self):  # stdin
        t = Timer(60, self.display_report)  # 60 seconds
        t.start()
        while True:
            input_str = raw_input("=> ")
            reader = csv.reader(StringIO.StringIO(input_str))

            input_item = []
            for rows in reader:
                for column in rows:
                    input_item.append(column)

            if len(input_item) != 4:
                print " => Invalid number of parameter"
                exit(1)

            if not input_item[3].isdigit():
                print " => Invalid bytes number"
                exit(1)

            self.insert_item(input_item[0], input_item[1], input_item[2], long(input_item[3]))

    def sort_items(self):
        self.data.sort(key=itemgetter(1))
        tmp_data = []
        for item, group in groupby(self.data, itemgetter(1)):
            tmp_data.append((item, sum(itemgetter(3)(row) for row in group)))
        return tmp_data

    def display_report(self):
        top_items = self.sort_items()
        top_items.sort(key=itemgetter(1), reverse=True)
        counter = 0
        for top_item in top_items:
            if counter >= 5:
                break

            print "{}. {} {} {} bytes".format(counter + 1, top_item[0], self.ips[top_item[0]], top_item[1])
            counter += 1


bytesReport = BytesReport()
bytesReport.simulate_streaming_input()
